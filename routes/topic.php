<?php

$this->group(['prefix' => '/topic'], function () {
    $this->get('/index', [
        'uses' => 'TopicController@index',
        'as' => 'topic.index',
    ]);
    $this->get('/create', [
        'uses' => 'TopicController@create',
        'as' => 'topic.create',
    ]);
    $this->post('/store', [
        'uses' => 'TopicController@store',
        'as' => 'topic.store',
    ]);

    $this->get('/edit/{id}', [
        'uses' => 'TopicController@edit',
        'as' => 'topic.edit',
    ])->where(['id' => '\d+']);

    $this->put('/update/{id}', [
        'uses' => 'TopicController@update',
        'as' => 'topic.update',
    ])->where(['id' => '\d+']);

    $this->patch('/toggle-publish/{id}', [
        'uses' => 'TopicController@togglePublish',
        'as' => 'topic.toggle_publish',
    ])->where(['id' => '\d+']);



    $this->delete('/destroy/{id}', [
        'uses' => 'TopicController@destroy',
        'as' => 'topic.destroy',
    ])->where(['id' => '\d+']);
});
