<?php

// Home
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Painel', route('dashboard'));
});


// User
Breadcrumbs::register('topic.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Lista de Temas', route('topic.index'));
});
Breadcrumbs::register('topic.create', function ($breadcrumbs) {
    $breadcrumbs->parent('topic.index');
    $breadcrumbs->push('Novo Tema', route('topic.create'));
});
Breadcrumbs::register('topic.edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('topic.index');
    $breadcrumbs->push('Editar Tema', route('topic.edit', $id));
});
Breadcrumbs::register('topic.edit_logged', function ($breadcrumbs) {
    $breadcrumbs->parent('topic.index');
    $breadcrumbs->push('Editar Senha', route('topic.edit_logged'));
});
