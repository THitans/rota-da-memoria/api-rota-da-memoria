<?php

Route::get('/', function () {
    return redirect('/login');
});

// Authentication Routes...
$this->get('login', 'LoginController@showLoginForm')->name('login');
$this->post('login', 'LoginController@login');
$this->post('logout', 'LoginController@logout')->name('logout');

$this->group(['prefix' => '', 'middleware' => ['auth:web']], function () {
    $this->get('/dashboard', 'DashboardController@index')->name('dashboard');

    include __DIR__ . '/topic.php';
    include __DIR__ . '/sub_topic.php';
});

// Add in blade template `<script src="/js/lang.js"></script>`
// https://medium.com/@serhii.matrunchyk/using-laravel-localization-with-javascript-and-vuejs-23064d0c210e
// Add in Vue: Vue.prototype.jsTrans = (string) => _.get(window.i18n, string);
$this->get('/js/lang.js', function () {
    // Para limpar o cache: php artisan cache:forget lang.js
//    $strings = Cache::remember('lang.js', 1, function () {
    $lang = config('app.locale');

    $files = glob(resource_path('lang/' . $lang . '/*.php'));
    $strings = [];

    foreach ($files as $file) {
        $name = basename($file, '.php');
        $strings[$name] = require $file;
    }

//        return $strings;
//    });

    header('Content-Type: text/javascript');
    echo('window.trans = ' . json_encode($strings) . ';');
    exit();
})->name('assets.lang');