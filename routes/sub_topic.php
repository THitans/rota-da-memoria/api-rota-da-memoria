<?php

$this->group(['prefix' => '/sub-topic'], function () {

    $this->get('/index/topic/{topicId}', [
        'uses' => 'SubTopicController@index',
        'as' => 'sub_topic.index',
    ]);

    $this->post('/store', [
        'uses' => 'SubTopicController@store',
        'as' => 'sub_topic.store',
    ]);

    $this->put('/update/{id}', [
        'uses' => 'SubTopicController@update',
        'as' => 'sub_topic.update',
    ])->where(['id' => '\d+']);

    $this->delete('/destroy/{id}', [
        'uses' => 'SubTopicController@destroy',
        'as' => 'sub_topic.destroy',
    ])->where(['id' => '\d+']);
});
