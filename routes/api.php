<?php

Route::post('login', 'AuthController@login');
Route::post('refresh', 'AuthController@refresh');

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::get('logout', 'AuthController@logout');

    Route::get('dashboard', 'DashboardController@index');

    Route::get('topics', 'TopicController@topics');

    Route::post('upload-image', 'UploadController@storeFile');

    Route::get('list-images/{subTopicId}', 'UploadController@listUrl');

    Route::get('download-image/{id}', 'UploadController@download');

    Route::post('comment/store', 'CommentController@store');
    Route::get('comment/list/{fileId}', 'CommentController@list');
});

