<?php

namespace App;

/**
 * @property int $id
 * @property int $topic_id
 * @property string $text
 * @property boolean $is_published
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property Topic $topic
 */
class SubTopic extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['topic_id', 'text', 'is_published'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function topic()
    {
        return $this->belongsTo('App\Topic');
    }

    public function files()
    {
        return $this->belongsToMany(File::class, 'sub_topic_file')->withTimestamps();
    }
}
