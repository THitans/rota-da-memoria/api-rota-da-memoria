<?php

namespace App;

/**
 * @property int $id
 * @property int $file_id
 * @property int $user_id
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property File $file
 * @property User $user
 */
class Comment extends BaseModel
{
    /**
     * @var array
     */
    protected $fillable = ['file_id', 'user_id', 'text'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('App\File');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
