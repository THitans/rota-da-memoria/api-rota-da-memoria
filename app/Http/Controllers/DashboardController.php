<?php

namespace App\Http\Controllers;


class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $countTopicsPublished = app('model.topic')->where('is_published', true)->count();
        $countTopicsNotPublished = app('model.topic')->where('is_published', false)->count();
        $countSubTopicsPublished = app('model.sub_topic')->where('is_published', true)->count();
        $countSubTopicsNotPublished = app('model.sub_topic')->where('is_published', false)->count();

        return view('dashboard', compact(
            'countTopicsPublished',
            'countTopicsNotPublished',
            'countSubTopicsPublished',
            'countSubTopicsNotPublished'
        ));
    }
}
