<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTopicRequest;
use App\Http\Requests\UpdateTopicRequest;

class TopicController extends Controller
{
    public function index()
    {
        try {
            $topics = app('model.topic')->orderBy('text')->get();

            return view('topic.index', compact('topics'));
        } catch (\Exception $exception) {
            flash('danger', trans('flash.index_error'), $exception->getMessage());

            return redirect()->route('dashboard');
        }
    }

    public function create()
    {
        try {
            return view('topic.create');
        } catch (\Exception $exception) {
            flash('danger', trans('flash.create_error'), $exception->getMessage());
            return redirect()->route('topic.index');
        }
    }

    public function store(CreateTopicRequest $request)
    {
        try {
            \DB::beginTransaction();

            $topic = app('model.topic')->fill($request->all());
            $topic->save();

            \DB::commit();

            flash('success', trans('flash.save_success'));

            return redirect()->route('topic.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('danger', trans('flash.save_error'), $e->getMessage());

            return \Redirect::route('topic.create')
                ->withErrors($request->validate())
                ->withInput();
        }
    }

    public function edit($id)
    {
        try {
            $topic = app('model.topic')->findOrFail($id);

            return view('topic.edit', compact('topic'));
        } catch (\Exception $exception) {
            flash('danger', trans('flash.edit_error'), $exception->getMessage());

            return redirect()->route('topic.index');
        }
    }

    public function update(UpdateTopicRequest $request, $id)
    {
        try {
            \DB::beginTransaction();

            $topic = app('model.topic')->findOrFail($id);
            $topic->update($request->all());

            \DB::commit();

            flash('success', trans('flash.update_success'));

            return redirect()->route('topic.index');
        } catch (\Exception $e) {
            \DB::rollBack();
            flash('danger', trans('flash.upload_errors'), $e->getMessage());

            return \Redirect::route('topic.index')
                ->withErrors($request->validate())
                ->withInput();
        }
    }

    public function togglePublish($id)
    {
        try {
            \DB::beginTransaction();

            $topic = app('model.topic')->findOrFail($id);
            $topic->is_published = !$topic->is_published;
            $topic->save();

            \DB::commit();

            return response()->json([
                'message' => $topic->is_published ? 'Tema Publicado' : 'Tema Salvo com Rascunho'
            ], 200);
        } catch (\Exception $e) {
            \DB::rollBack();
            
            return response()->json([
                'message' => 'Erro ao Atualizar Tema'
            ], 500);
        }
    }
}