<?php

namespace App\Http\Controllers\API;

use App\Exceptions\APIException;
use App\Http\Requests\API\LoginUserRequest;
use Illuminate\Http\Request;
use JWTAuthException;
use JWTAuth;

class AuthController extends Controller
{
    public function login(LoginUserRequest $request)
    {
        if (!$token = JWTAuth::attempt($request->only('email', 'password'))) {
            $message = trans('jwt.invalid_login');
            throw new APIException($message, 401);
        }

        return response()->json([
            'message' => trans('jwt.login_success'),
            'user' => app('model.user')->userLogged($request->email)->first(),
            'token' => $token
        ]);       
    }

    public function refresh(Request $request)
    {
            $token = JWTAuth::refresh($request->token);

            return response()->json([
                'message' => trans('jwt.token_refreshed'),
                'token' => $token
            ]);
    }
}
