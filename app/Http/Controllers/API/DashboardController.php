<?php

namespace App\Http\Controllers\API;

class DashboardController extends Controller
{
    public function index()
    {
        return response()->json('Dashboard', 200);
    }
}
