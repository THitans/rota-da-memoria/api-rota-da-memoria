<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCommentRequest;
use Illuminate\Database\QueryException;

class CommentController extends Controller
{
    public function list($fileId)
    {
        $comments = app('model.comment')->with('user')->where('file_id', $fileId)->get();

        return response()->json([
            'comments' => $comments
        ], 200);
    }

    public function store(CreateCommentRequest $request)
    {
        try {
            \DB::beginTransaction();

            app('model.comment')->create($request->all());

            \DB::commit();

            return response()->json([
                'message' => 'Comentário salvo com Sucesso!'
            ], 200);
        } catch (QueryException $queryException) {
            \DB::rollBack();

            return response()->json([
                'message' => 'Verifique os dados que enviou. Pricipalmente os IDs. Se o erro persistir Entre em contato com o Desenvolvedor',
                'exception' => $queryException->getMessage()
            ], 400);

        } catch (\Exception $exception) {
            \DB::rollBack();

            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }
}
