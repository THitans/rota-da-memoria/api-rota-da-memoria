<?php

namespace App\Http\Controllers\API;

class TopicController extends Controller
{
    public function topics()
    {
        $topics = app('model.topic')->with('subTopics')->get();

        return response()->json($topics);
    }
}
