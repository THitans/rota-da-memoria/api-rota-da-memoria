<?php

namespace App\Http\Controllers\API;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    private function saveFile($request, $path)
    {
        $file = app('model.file');
        $file->name = $request->file('file')->getClientOriginalName();
        $file->file_name = $request->file('file')->getFilename();
        $file->extension = $request->file('file')->getClientOriginalExtension();
        $file->path_file = $path;
        $file->mimetype_file = $request->file('file')->getMimeType();
        $file->size = $request->file('file')->getSize();
        $file->save();

        return $file;
    }

    public function storeFile(Request $request)
    {
        try {
            \DB::beginTransaction();

            $this->validate($request, [
                'file' => 'required|mimes:gif,jpg,jpeg,png,webp|max:10000000',
            ]);

            $path = \Storage::disk('public')->putFile('sub_topic', $request->file('file'));
            $file = $this->saveFile($request, $path);
            $subTopic = app('model.sub_topic')->findOrFail($request->get("sub_topic_id"));
            $subTopic->files()->attach($file->id);

            \DB::commit();

            return response()->json([
                'message' => trans('flash.upload_success'),
                'data' => $file
            ], 200);
        } catch (QueryException $queryException) {
            \DB::rollBack();

            return response()->json([
                'message' => 'Verifique os dados que enviou. Pricipalmente os IDs. Se o erro persistir Entre em contato com o Desenvolvedor',
                'exception' => $queryException->getMessage()
            ], 400);

        } catch (\Exception $exception) {
            \DB::rollBack();

            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function listUrl($subTopicId)
    {
        try {
            $files = app('model.file')->findBySubTopic($subTopicId)->get()->toArray();

            return response()->json([
                'basePath' => \URL::to('/storage/'),
                'files' => $files
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function download($id)
    {
        try {
            $_file = app('model.file')->findOrFail($id);

            $file = Storage::download($_file->path_file);

            return $file;
        } catch (\Exception $exception) {
            return response()->json([
                'message' => $exception->getMessage()
            ], 500);
        }
    }
}
