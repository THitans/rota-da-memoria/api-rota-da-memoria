<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSubTopicRequest;
use App\Http\Requests\UpdateSubTopicRequest;
use App\SubTopic;

class SubTopicController extends Controller
{
    public function index($topicId)
    {
        try {
            $subTopics = app('model.sub_topic')->where('topic_id', $topicId)->get();

            return response()->json(compact('subTopics'), 200);
        } catch (\Exception $exception) {
            flash('danger', trans('flash.index_error'), $exception->getMessage());

            return redirect()->route('dashboard');
        }
    }

    public function store(CreateSubTopicRequest $request)
    {
        try {
            \DB::beginTransaction();

            $subTopic = app('model.sub_topic')->create($request->all());

            \DB::commit();

            return response()->json([
                'subTopic' => $subTopic,
                'message' => 'Sucesso'
            ], 200);
        } catch (\Exception $exception) {
            \DB::rollBack();

            return response()->json([
                'success' => false,
                'route' => route('topic.index'),
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function destroy($id)
    {
        try {
            $object = SubTopic::findOrfail($id);
            $object->delete();
            return response()->json(['success' => true]);
        } catch (\Exception $exception) {
            return response()->json([
                'success' => false,
                'route' => route('topic.index'),
                'message' => $exception->getMessage()
            ], 500);
        }
    }

    public function update(UpdateSubTopicRequest $request, $id)
    {
        try {
            \DB::beginTransaction();

            $subTopic = app('model.sub_topic')->findOrFail($id);
            $subTopic->update($request->all());

            \DB::commit();

            return response()->json([
                'subTopic' => $subTopic,
                'message' => 'Sucesso'
            ], 200);

        } catch (\Exception $e) {
            \DB::rollBack();

            return response()->json([
                'success' => false,
                'route' => route('topic.index'),
                'message' => $e->getMessage()
            ], 500);
        }
    }

    public function listImages($subtopicId)
    {

    }
}