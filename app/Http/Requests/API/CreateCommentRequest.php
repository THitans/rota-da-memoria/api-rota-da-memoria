<?php

namespace App\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class CreateCommentRequest extends FormRequest
{
    public function authorize()
    {
        $fields = ['file_id', 'user_id', 'text'];

        /**
         * Se não passar todos os valores exigidos no post lança Exceção
         */
        return verify_fields_request($fields, app('request')->all());
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_id' => 'required|integer',
            'user_id' => 'required|integer',
            'text' => 'required|string|min:6|max:255',
        ];
    }
}
