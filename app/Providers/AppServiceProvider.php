<?php

namespace App\Providers;

use App\Comment;
use App\File;
use App\SubTopic;
use App\Topic;
use Illuminate\Support\ServiceProvider;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('model.user', User::class);
        $this->app->bind('model.topic', Topic::class);
        $this->app->bind('model.sub_topic', SubTopic::class);
        $this->app->bind('model.comment', Comment::class);
        $this->app->bind('model.file', File::class);
    }
}
