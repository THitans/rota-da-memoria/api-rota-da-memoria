<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $text
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property SubTopic[] $subTopics
 */
class Topic extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['text', 'is_published'];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subTopics()
    {
        return $this->hasMany('App\SubTopic');
    }
}
