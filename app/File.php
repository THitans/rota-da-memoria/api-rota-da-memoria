<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 * @property string $extension
 * @property string $file_name
 * @property string $path_file
 * @property string $mimetype_file
 * @property int $size
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property SubTopicFile[] $subTopicFiles
 * @property Comment[] $comments
 */
class File extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'extension',
        'file_name',
        'path_file',
        'mimetype_file',
        'size'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subTopicFiles()
    {
        return $this->hasMany(SubTopic::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function findBySubTopic($subTopicId)
    {
        return $this->select('files.id', 'files.file_name', 'files.path_file', 'files.created_at', 'files.deleted_at')
            ->rightJoin('sub_topic_file', 'files.id', '=', 'sub_topic_file.file_id')
            ->where('sub_topic_file.sub_topic_id', $subTopicId);
    }
}
