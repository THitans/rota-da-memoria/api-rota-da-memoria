<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class TopicsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        for ($i = 0; $i < 10; $i++) {
            $topic = app('model.topic')->fill([
                'text' => $faker->text(rand(20, 100)),
                'is_published' => $i % 2 == 0 ? true : false
            ]);
            $topic->created_at = $faker->date();
            $topic->save();

            for ($j = 0; $j < rand(0, 10); $j++) {
                $subTopic = app('model.sub_topic')->fill([
                    'text' => $faker->text(rand(20, 100)),
                    'is_published' => $j % 2 == 0 ? true : false,
                    'topic_id' => $topic->id
                ]);
                $subTopic->created_at = $faker->date();
                $subTopic->save();

            }
        }
    }
}
