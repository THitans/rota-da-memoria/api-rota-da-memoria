<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        if ( ! App\User::where('email', '=', '91120802334')->exists()) {
            factory(App\User::class)->create();
        }

        foreach ($this->getUsersData() as $data) {

            if ( ! app('model.user')->where('email', '=', $data['cpf'])->exists()) {
                app('model.user')->updateOrCreate([
                    'name' => $data['name'],
                    'email' => $data['cpf'],
                    'password' => $data['cpf'],
                    'type' => 'user',
                    'remember_token' => str_random(10),
                ]);
            }
        }

    }

    private function getUsersData()
    {
        return [
            [
                'name' => 'Ana Luiza Alves Moreira',
                'cpf' => '06299706104',
            ],
            [
                'name' => 'Gustavo Bonifácio Ramos',
                'cpf' => '05759639137',
            ],
            [
                'name' => 'Victor Eduarco Ribeiro Maciel',
                'cpf' => '08037264142',
            ],
            [
                'name' => 'Ruan Pablo Vogado Silva',
                'cpf' => '07446589183',
            ],
            [
                'name' => 'Gabriel Milhomem',
                'cpf' => '04295696102',
            ],
            [
                'name' => 'Antônia Beatriz',
                'cpf' => '07185547105',
            ],
            [
                'name' => 'Lais Lopes FErreira',
                'cpf' => '06737592104',
            ],
            [
                'name' => 'Marcos Vinicius R. C. Araújo',
                'cpf' => '04510018185',
            ],
            [
                'name' => 'Pedro Henrique Ribeiro',
                'cpf' => '06465320142',
            ],
            [
                'name' => 'Débora Ribeiro Pereira',
                'cpf' => '08031128154',
            ],
            [
                'name' => 'Thais Ferreira Almeida',
                'cpf' => '09865941160',
            ],
            [
                'name' => 'Yasmin dos Santos Alves',
                'cpf' => '06662539198',
            ],
            [
                'name' => 'Raika Gomes de Almeida',
                'cpf' => '06287870141',
            ],
            [
                'name' => 'Tamires Carvalho',
                'cpf' => '06465738104',
            ],
            [
                'name' => 'Gabriel de Carvalho',
                'cpf' => '07678853111',
            ],
            [
                'name' => 'Gabriela Costa',
                'cpf' => '08023613125',
            ],
            [
                'name' => 'Micaele Alves dos Santos',
                'cpf' => '06025090173',
            ],
            [
                'name' => 'Mayke Melo Santana',
                'cpf' => '08064771137',
            ],
            [
                'name' => 'Victor Eduardo',
                'cpf' => '08051264142',
            ],
            [
                'name' => 'Gabriela Lorandos',
                'cpf' => '08043613125',
            ],
            [
                'name' => 'Alicia Pacheco',
                'cpf' => '0610363956',
            ]
        ];
    }
}
