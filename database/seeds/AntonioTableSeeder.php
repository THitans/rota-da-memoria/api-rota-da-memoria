<?php

use Illuminate\Database\Seeder;

class AntonioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app('model.user')->updateOrCreate([
            'name' => 'Antonio Guanacuy',
            'email' => '00440579384',
            'password' => '00440579384',
            'type' => 'admin',
            'remember_token' => str_random(10),
        ]);
    }
}
