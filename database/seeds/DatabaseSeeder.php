<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (isEnv('local')) {
            $this->call([
                UsersTableSeeder::class,
                TopicsTableSeeder::class
            ]);
        } else {
            $this->call([
                UsersTableSeeder::class,
            ]);            
        }
    }
}
