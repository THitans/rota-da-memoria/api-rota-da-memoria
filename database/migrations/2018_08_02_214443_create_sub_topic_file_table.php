<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubTopicFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_topic_file', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('sub_topic_id')->unsigned();
            $table->foreign('sub_topic_id')
                ->references('id')
                ->on('sub_topics');


            $table->integer('file_id')->unsigned();
            $table->foreign('file_id')
                ->references('id')
                ->on('files');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_topic_file');
    }
}
