// (function() {
//
//     var bodyEl = $('body'),
//         navToggleBtn = bodyEl.find('.nav-toggle-btn');
//
//     navToggleBtn.on('click', function(e) {
//         bodyEl.toggleClass('active-nav');
//         e.preventDefault();
//     });
//
//
//
// })();
//
// (function() {
//     $(function() {
//         var collapseMyMenu, expandMyMenu, hideMenuTexts, showMenuTexts;
//         expandMyMenu = function() {
//             return $("nav.sidebar").removeClass("sidebar-menu-collapsed").addClass("sidebar-menu-expanded");
//         };
//         collapseMyMenu = function() {
//             return $("nav.sidebar").removeClass("sidebar-menu-expanded").addClass("sidebar-menu-collapsed");
//         };
//         showMenuTexts = function() {
//             return $("nav.sidebar ul a span.expanded-element").show();
//         };
//         hideMenuTexts = function() {
//             return $("nav.sidebar ul a span.expanded-element").hide();
//         };
//         return $("#justify-icon").click(function(e) {
//             if ($(this).parent("nav.sidebar").hasClass("sidebar-menu-collapsed")) {
//                 expandMyMenu();
//                 showMenuTexts();
//                 $(this).css({
//                     color: "#000"
//                 });
//             } else if ($(this).parent("nav.sidebar").hasClass("sidebar-menu-expanded")) {
//                 collapseMyMenu();
//                 hideMenuTexts();
//                 $(this).css({
//                     color: "#FFF"
//                 });
//             }
//             return false;
//         });
//     });
//
// }).call(this);

(function($) {
    "use strict"; // Start of use strict

    // Toggle the side navigation
    $("#sidenavToggler").click(function(e) {
        e.preventDefault();
        $("body").toggleClass("sidenav-toggled");
        $(".navbar-sidenav .nav-link-collapse").addClass("collapsed");
        $(".navbar-sidenav .sidenav-second-level, .navbar-sidenav .sidenav-third-level").removeClass("show");
    });
    //Force the toggled class to be removed when a collapsible nav link is clicked
    $(".navbar-sidenav .nav-link-collapse").click(function(e) {
        e.preventDefault();
        $("body").removeClass("sidenav-toggled");
    });
    // // Prevent the content wrapper from scrolling when the fixed side navigation hovered over
    $('body.fixed-nav .navbar-sidenav, body.fixed-nav .sidenav-toggler, body.fixed-nav .navbar-collapse').on('mousewheel DOMMouseScroll', function(e) {
        var e0 = e.originalEvent,
            delta = e0.wheelDelta || -e0.detail;
        this.scrollTop += (delta < 0 ? 1 : -1) * 30;
        e.preventDefault();
    });
})(jQuery); // End of use strict

$('#toggleNavPosition').click(function() {
    $('body').toggleClass('fixed-nav');
    $('nav').toggleClass('fixed-top static-top');
});
