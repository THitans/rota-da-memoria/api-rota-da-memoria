
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import BootstrapVue from 'bootstrap-vue';

/**
 * https://github.com/shakee93/vue-toasted
 */
import Toasted from 'vue-toasted';

require('./bootstrap');

window.Vue = require('vue');
Vue.use(BootstrapVue);
Vue.use(VueMoment, {
    moment
});

/**
 * https://github.com/shakee93/vue-toasted
 * Lista de ícones: https://fontawesome.com/cheatsheet
 */
Vue.use(Toasted, {
    iconPack : 'fontawesome'
});

/**
 * Para utilizar o mesmo helper jsTrans() no javascript
 * https://medium.com/@serhii.matrunchyk/using-laravel-localization-with-javascript-and-vuejs-23064d0c210e
 */
Vue.prototype.jsTrans = (string) => _.get(window.trans, string);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('trash-btn', require('./components/trashBtn/trashBtn.vue'));
Vue.component('crud-sub-topic', require('./components/subTopic/subTopic.vue'));
Vue.component('on-off-checkbox', require('./components/onOffCheckbox/onOffCheckbox.vue'));

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('MM/DD/YYYY hh:mm')
    }
});


const app = new Vue({
    el: '#app'
});

require('./custom/nav');
