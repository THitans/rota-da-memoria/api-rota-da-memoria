<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Courses">
    <a class="nav-link nav-link-collapse" data-toggle="collapse"
       href="#collapseTopic" data-parent="#exampleAccordion">
        <i class="fa fa-fw fa-graduation-cap"></i>
        <span class="nav-link-text">Temas</span>
    </a>
    <ul class="sidenav-second-level collapse {{ menuActive(['topic.index', 'topic.create'], Route::currentRouteName(), 2) }}"
        id="collapseTopic">

        <li class="{{ menuActive(['topic.index', 'topic.create', 'topic.edit', 'topic.checklist'], Route::currentRouteName(), 3) }}">
            <a class="nav-link" href="{{ route('topic.index') }}">
                <i class="fa fa-fw fa-arrow-right"></i>
                <span class="nav-link-text">Listar</span>
            </a>
        </li>
    </ul>
</li>