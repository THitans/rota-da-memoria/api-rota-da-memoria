<form id="form-student-create" action="{{ $route }}" method="POST" novalidate="novalidate">
{{ csrf_field() }}
<button class="btn btn-dark btn-sm" type="submit" id="btnRestore"><i class="fa fa-window-restore"></i></button>
<b-tooltip target="btnRestore" title="Restaurar {{$model}}"></b-tooltip>
</form>