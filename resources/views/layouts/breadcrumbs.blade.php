@if (count($breadcrumbs))
    <div class="container-fluid">
        <div class="breadcrumb mt-2 mb-0">
            @foreach ($breadcrumbs as $breadcrumb)
                @if ($breadcrumb->url && !$loop->last)
                    <a class="breadcrumb__step" href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                @else
                    <span class="breadcrumb__step breadcrumb__step--active" href="#">{{ $breadcrumb->title }}</span>
                @endif
            @endforeach
        </div>
    </div>
@endif