<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="fixed-nav sticky-footer bg-blue" id="page-top">

@include('layouts.nav.index')

<div id="app" class="content-wrapper">
    <div class="container-fluid">
        {{ Breadcrumbs::render() }}

        @include('alerts.flashMessage')
        @yield('content')
    </div>
</div>
<footer class="sticky-footer">
    <div class="container">
        <div class="text-center">
            <div class="copy-text">Dev <i class="fa fa-code"></i> by <strong><a href="#">THITANS</a></strong></div>
        </div>
    </div>
</footer>

<!-- Scripts -->
<script src="{{ asset('/js/manifest.js') }}"></script>
<script src="{{ asset('/js/vendor.js') }}"></script>
<script src="{{ asset('/js/app.js') }}"></script>
<script src="{{ asset('/js/lang.js') }}"></script>
</body>
</html>
