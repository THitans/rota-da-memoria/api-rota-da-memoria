@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="card mb-4 card-shadow-6">
            <h5 class="card-header"><strong>Novo</strong> Tema</h5>
            <div class="card-body">
                <form id="form-topic-create" method="POST" action="{{ route('topic.store')}}" novalidate="novalidate">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8 offset-md-2">
                            <label for="alias">Título</label>
                            <input type="text"
                                   name="text"
                                   class="form-control {{ isInvalid($errors, 'alias') }}"
                                   id="alias"
                                   placeholder="Digite o Título do Tema"
                                   value="{{ old('text') }}"
                                   autofocus>
                            @include('alerts.input', ['name' => 'text'])
                        </div>
                    </div>
                </form>
            </div>
            <div class="card-footer">
                <div class="col-md-8 offset-md-2">
                    <div class="button-group pull-right">
                        <button type="submit" form="form-topic-create" class="btn btn-primary btn-right">{{ trans('forms.button_cad') }}</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection