@extends('layouts.app')

@section('content')

    <div class="container-fluid">
        <div class="card mb-4 card-shadow-6">
            <div class="card-header">
                <h5 class="title"><strong>Lista</strong> de Temas</h5>
                <div class="pull-right">

                    <a href="{{ route('topic.create') }}" class="btn btn-primary btn-sm">
                        <i class="fa fa-plus"></i> {{ trans('forms.button_new') }}
                    </a>

                </div>
            </div>
            <div class="card-body">

                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Data de Criação</th>
                            <th scope="col">Título</th>
                            <th scope="col" class="text-center">Status</th>
                            <th scope="col" class="text-center">Subtemas Temas</th>
                            <th class="text-center"><i class="fa fa-cog"></i></th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($topics as $data)
                            <tr class="{{ deletedAt($data->deleted_at, 1) }}">
                                <td>{!! getId($data->id, $loop->iteration) !!}</td>
                                <td>{!! $data->created_at->format('d/m/Y') !!}</td>
                                <td>{!! $data->text !!}</td>
                                <td class="text-center">
                                    <on-off-checkbox 
                                        data-id="{{ $data->id }}"
                                        data-label-on="Publicado" 
                                        data-label-off="Rascunho"  
                                        data-is-checked="{{ $data->is_published }}"
                                        data-url="{{ route('topic.toggle_publish', $data->id) }}">
                                    </on-off-checkbox>
                                </td>
                                <td class="text-center">
                                    <span class="badge badge-success p-2">{{ $data->subTopics()->count() }}</span>
                                </td>
                                @if(!deletedAt($data->deleted_at, 2))

                                    <td class="text-center">
                                        <a href="{{ route('topic.edit', ['id' => $data->id]) }}"
                                           class="btn btn-warning btn-sm"><i class="fa fa-edit"></i>
                                        </a>

                                        <trash-btn href="{{ route('topic.destroy', $data->id) }}"
                                                   data-id="{{ $data->id }}">
                                        </trash-btn>
                                    </td>
                                @elseif (deletedAt($data->deleted_at, 1))
                                    <td class="text-center">
                                        @include('layouts.btnRestore', [
                                            'route' => route('topic.restore', ['id' => $data->id]),
                                            'model' => 'Curso'
                                        ])
                                    </td>

                                @endif


                            </tr>
                        @empty
                            <tr class="text-center">
                                <td colspan="6">{{ trans('list.empty') }}</td>
                            </tr>
                        @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection