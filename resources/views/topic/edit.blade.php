@extends('layouts.app')

@section('content')

    <div class="container-fluid">

        <div class="card mb-4 card-shadow-6">
            <h5 class="card-header"><strong>Editar</strong> Tema - <span class="badge badge-blue"><small>{{ $topic->text }}</small></span></h5>
            <div class="card-body">
                <form id="form-topic-edit" action="{{ route('topic.update', $topic->id)}}" method="POST" novalidate="novalidate">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">
                    <div class="row">
                        <div class="col-md-8 offset-md-1">
                            <label for="alias">Título</label> <span class="badge badge-warning">Criado em: {{ $topic->created_at->format('d/m/Y h:m') }}</span>
                            <input type="text"
                                   name="text"
                                   class="form-control {{ isInvalid($errors, 'alias') }}"
                                   id="alias"
                                   placeholder="Digite o Título do Tema"
                                   value="{{ empty(old('text')) == false ? old('text') : $topic->text }}"
                                   autofocus>
                            @include('alerts.input', ['name' => 'text'])
                        </div>
                        <div class="col-md-3">

                            <label for="alias">Status</label>
                            <on-off-checkbox 
                                data-id="{{ $topic->id }}"
                                data-label-on="Publicado" 
                                data-label-off="Rascunho"  
                                data-is-checked="{{ $topic->is_published }}"
                                data-url="{{ route('topic.toggle_publish', $topic->id) }}">
                            </on-off-checkbox>
                        </div>
                    </div>

                </form>
            </div>

            <div class="card-footer">
                <div class="button-group pull-right">
                    <button type="submit" form="form-topic-edit" class="btn btn-primary btn-right">{{ trans('forms.button_update') }}</button>
                </div>
            </div>
        </div>
        <crud-sub-topic topic-id="{{ $topic->id }}"></crud-sub-topic>
    </div>
@endsection