@extends('layouts.base')

@section('content')
    <section class="login-block">
        <div class="container ctn-custom">
            <div class="row">
                <div class="col-md-4 login-sec">
                    <h2 class="text-center"><img class="img-logo" src="/img/logo_positiva.png"></h2>
                    <form class="login-form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="exampleInputEmail1" class="text-uppercase">CPF</label>
                            <input id="email" type="text"
                                   class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder=""
                                   name="email" value="{{ old('email') }}"
                                   maxlength="11"
                                   required
                                   autofocus>
                            @if ($errors->has('email'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1" class="text-uppercase">Senha</label>
                            <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="" name="password" required>
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </div>
                            @endif
                        </div>


                        <div class="form-check">
                            <label class="form-check-label">
                                <input type="checkbox" class="form-check-input" {{ old('remember') ? 'checked' : '' }}>
                                <small>Lembrar-me</small>
                                <br>
                                {{--<small><a class="login-link" href="{{ route('password.request') }}">--}}
                                        {{--Esqueceu a Senha?--}}
                                    {{--</a></small>--}}
                            </label>
                            <button type="submit" class="btn btn-login float-right">Entrar</button>
                        </div>

                    </form>
                    <div class="copy-text">Dev <i class="fa fa-code"></i> by <strong><a href="#">THitans</a></strong>
                    </div>
                </div>
                <div class="col-md-8 banner-sec">
                    <div id="carouselExampleIndicators" class="carousel slide full-height-important" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                        </ol>
                        <div class="carousel-inner full-height-important" role="listbox">
                            <div class="carousel-item active full-height-important">
                                <img class="img-carousel"
                                     src="https://static.pexels.com/photos/33972/pexels-photo.jpg" alt="First slide">
                                <div class="carousel-caption d-none d-md-block">
                                    <div class="banner-text text-left">
                                        <h2>Rota da Memória - Admin</h2>
                                        <p>Nome da Cliente</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
