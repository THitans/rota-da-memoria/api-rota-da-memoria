@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-12 col-md-3 col-sm-4">
                <div class="small-card bg-yellow">
                    <div class="inner">
                        <h3>{{ $countTopicsPublished }}</h3>
                        <p>Temas Publicado</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list-ol"></i>
                    </div>
                    <a href="{{ route('topic.index') }}" class="small-card-footer">Ver mais
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-12 col-md-3 col-sm-4">
                <div class="small-card bg-gray">
                    <div class="inner">
                        <h3>{{ $countTopicsNotPublished }}</h3>
                        <p>Temas Não Publicado</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list-ol"></i>
                    </div>
                    <a href="{{ route('topic.index') }}" class="small-card-footer">Ver mais
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-12 col-md-3 col-sm-4">
                <div class="small-card bg-blue-strong">
                    <div class="inner">
                        <h3>{{ $countSubTopicsPublished }}</h3>
                        <p>Sub Temas</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list"></i>
                    </div>

                    <a href="{{ route('topic.index') }}" class="small-card-footer">Ver mais
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-12 col-md-3 col-sm-4">
                <div class="small-card bg-gray">
                    <div class="inner">
                        <h3>{{ $countSubTopicsNotPublished }}</h3>
                        <p>Sub Temas Não Publicados</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list"></i>
                    </div>

                    <a href="{{ route('topic.index') }}" class="small-card-footer">Ver mais
                        <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            {{--<div class="col-3">--}}
                {{--<div class="small-card bg-light-blue">--}}
                    {{--<div class="inner">--}}
                        {{--<h3>{{ 10 }}</h3>--}}
                        {{--<p>Comentários</p>--}}
                    {{--</div>--}}
                    {{--<div class="icon">--}}
                        {{--<i class="fa fa-users"></i>--}}
                    {{--</div>--}}

                    {{--<a href="#" class="small-card-footer">Ver mais--}}
                        {{--<i class="fa fa-arrow-circle-right"></i>--}}
                    {{--</a>--}}
                {{--</div>--}}
            {{--</div>--}}

        </div>
    </div>
@endsection
