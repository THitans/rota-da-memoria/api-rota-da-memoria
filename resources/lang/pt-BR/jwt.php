<?php

return [
    'token_invalid' => 'Token Inválido!',
    'token_expired' => 'Token Expirado!',
    'token_refreshed' => 'Token Revalidado!',
    'error_refreshed' => 'Erro ao Revalidar o Token!',
    'invalid_login' => 'Email e/ou senha incorretos',
    'token_create_fail'=> 'Falha ao Criar Token',

    'login_success' => 'O login foi realizado com sucesso.'
];
